# isl-gyromag-ahrs

Project aimed to integrate gyroscope and magnetometer data to determine spatial orientation angles.

Authors:
- Karol Bresler
- Kamil Dąbrowski
- [Marek Łukasiewicz](http://lukasiewicz.tech/)
- Dariusz Miedziński

Supervisor: [mgr inż. Sebastian Topczewski](https://www.meil.pw.edu.pl/zaiol/ZAiOL/Pracownicy2/Sebastian-Topczewski)

Project made for the Integration of Aerospace Systems subject, carried out at the Faculty of Power and Aerospace Engineering, Warsaw University of Technology in the summer semester of 2018/2019.

# Tools used

- Version control: [Git](https://git-scm.com/)
- Documentation: [LaTeX](https://www.latex-project.org/)
- Computation:
    - Interpreted language [Python 3.7](https://www.python.org/)
    - Computational libraries [numpy](http://www.numpy.org/), [scipy](https://www.scipy.org/)
    - Visualization [matplotlib](https://matplotlib.org/)
- Tools:
    - Diagramming and flowcharting software [draw.io](https://www.draw.io/)
    - Other illustrations in [Inkscape](https://inkscape.org/)
    - LaTeX editor [TeXstudio](https://www.texstudio.org/)
    - Python editor [PyCharm](https://www.jetbrains.com/pycharm/)

# Useful links

- The syntax of this document: [tutorial from GitLab](https://about.gitlab.com/handbook/product/technical-writing/markdown-guide/)
- Window client for git [Github Desktop](https://desktop.github.com/)
    - [how to clone a repository](https://help.github.com/en/desktop/contributing-to-projects/cloning-a-repository-from-github-desktop)
    in step 2 select the URL tab and paste the HTTPS link of this repo (blue Clone button in the top right corner of this page)
- Files you don't want to sync are added to `.gitignore` [like this](https://git-scm.com/docs/gitignore)
- A post on tex.stackoverflow](https://tex.stackexchange.com/questions/41808/how-do-i-install-tex-latex-on-windows) about how to get started with LaTeX on Windows
- IDE for Tex that was used: [TeXstudio](https://www.texstudio.org/)
- A site to download .bib data [Citation Machine](http://www.citationmachine.net/)
- The tutorial I started with (Marek) [NotSoShort Introduction to LaTeX](https://tobi.oetiker.ch/lshort/lshort.pdf)
- Interactive video tutorial [quatered from 3blue1brown](https://eater.net/quaternions)
- Ardupilot is the most stable UAV autopilot because it has the best [Extended Kalman Filter](http://ardupilot.org/dev/docs/ekf.html)


# Article sources:

- [Institute of Electrical and Electronics Engineers](https://www.ieee.org/)
- [American Institute of Aeronautics and Astronautics](https://www.aiaa.org/)
- [Annual of Navigation](http://www.annualofnavigation.pl/)

# Podsumowanie

Thank you for a semester of collaboration on this subject.

## Comments to final report and presentation:

- You can use the [World Magnetic Model](https://www.ngdc.noaa.gov/geomag/WMM/DoDWMM.shtml)
- In the multimedia presentation itself there should be one graph per slide to make it readable
- Slide numbers are missing

# Documentation

Full report is available only in Polish. 
