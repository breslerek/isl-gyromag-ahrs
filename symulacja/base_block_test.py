import unittest
from base_block import BaseBlock


class BaseBlockTestCase(unittest.TestCase):
    def test_result(self):
        block = BaseBlock()

        self.assertEqual(5, block.get_result(5))


if __name__ == '__main__':
    unittest.main()
