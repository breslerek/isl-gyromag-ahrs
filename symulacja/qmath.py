import numpy as np


def normalized(a: np.ndarray):
    return a / np.sqrt(np.sum(a * a))


def quat_mul(q_a, q_b):
    """
    Hamilton product
    :param q_a: i, j, k, w left-hand quaternion
    :param q_b: i, j, k, w right-hand quaternion
    :return: q_a * q_b
    """
    b1, c1, d1, a1 = q_a
    b2, c2, d2, a2 = q_b
    return np.array([
        a1 * b2 + b1 * a2 + c1 * d2 - d1 * c2,
        a1 * c2 - b1 * d2 + c1 * a2 + d1 * b2,
        a1 * d2 + b1 * c2 - c1 * b2 + d1 * a2,
        a1 * a2 - b1 * b2 - c1 * c2 - d1 * d2
    ])


def rotate(v: np.ndarray, q: np.ndarray):
    p = np.append(v, 0)
    q_inv = q * [-1, -1, -1, 1]
    p_rotated = quat_mul(quat_mul(q, p), q_inv)
    return p_rotated[:3]


def quat2att(q: np.ndarray):
    """
    :param q: quaternion i,j,k,1
    :return: roll, pitch, yaw
    """

    roll = np.arctan2(2 * (q[0] * q[3] + q[1] * q[2]), - q[0] * q[0] - q[1] * q[1] + q[2] * q[2] + q[3] * q[3])
    pitch = -np.arcsin(np.clip(2 * (q[0] * q[2] - q[1] * q[3]), -1, 1))
    yaw = np.arctan2(2 * (q[0] * q[1] + q[2] * q[3]), + q[0] * q[0] - q[1] * q[1] - q[2] * q[2] + q[3] * q[3])
    return np.array([roll, pitch, yaw])


def slerp(v0, v1, t_array):
    # >>> slerp([1,0,0,0],[0,0,0,1],np.arange(0,1,0.001))

    t_array = np.array(t_array)
    v0 = np.array(v0)
    v1 = np.array(v1)
    dot = np.sum(v0 * v1)
    if dot < 0.0:
        v1 = -v1
        dot = -dot

    dot_equal_threshold = 0.9995
    if dot > dot_equal_threshold:
        result = v0[np.newaxis, :] + t_array[:, np.newaxis] * (v1 - v0)[np.newaxis, :]
        result = result / np.linalg.norm(result)
        return result

    theta_0 = np.arccos(dot)
    sin_theta_0 = np.sin(theta_0)
    theta = theta_0 * t_array
    sin_theta = np.sin(theta)
    s0 = np.cos(theta) - dot * sin_theta / sin_theta_0
    s1 = sin_theta / sin_theta_0
    # return (s0[:, np.newaxis] * v0[np.newaxis, :]) + (s1[:, np.newaxis] * v1[np.newaxis, :])
    return (s0 * v0) + (s1 * v1)

if __name__ == '__main__':
    a = 180
    c = np.cos(np.deg2rad(a) / 2)
    s = np.sin(np.deg2rad(a) / 2)
    # q = np.array([0, s, 0, c])
    r = np.array([1, 1, 0])
    q = np.append(normalized(r) * s, c)
    print(q)
    print(np.rad2deg(quat2att(q)))
