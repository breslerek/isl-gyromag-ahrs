import numpy as np
from base_block import BaseBlock
from dewiacja import dewiacja


class MagnetometrBlok(BaseBlock):
    def __init__(self):
        super(MagnetometrBlok, self).__init__()
        self.plik = 'dane/magnetometr.txt'
        self.N = 9
        self.modul_wektora = np.sqrt(3)

        self.bledy = None
        self.Km_inv = None
        self.bm = None

    def load(self):
        self.bledy = dewiacja(self.plik, self.N, self.modul_wektora)
        # print(self.bledy)

        eps1 = self.bledy[0]
        eps2 = self.bledy[1]
        eps3 = self.bledy[2]
        rho1 = self.bledy[3]
        rho2 = self.bledy[4]
        rho3 = self.bledy[5]
        dzeta1 = self.bledy[6]
        dzeta2 = self.bledy[7]
        dzeta3 = self.bledy[8]

        det_km = (eps1 * eps2 * np.cos(rho1) * eps3 * np.cos(rho2) * np.cos(rho3))
        a1 = (eps2 * np.cos(rho1) * eps3 * np.cos(rho2) * np.cos(rho3))
        a2 = (-eps2 * np.sin(rho1) * eps3 * np.cos(rho2) * np.cos(rho3))
        a3 = (eps2 * np.sin(rho1) * eps3 * np.sin(rho3) - eps2 * np.cos(
            rho1) * eps3 * np.sin(rho2) * np.cos(rho3))
        a5 = (eps1 * eps3 * np.cos(rho2) * np.cos(rho3))
        a6 = (-eps1 * eps3 * np.sin(rho3))
        a9 = (eps1 * eps2 * np.cos(rho1))

        self.Km_inv = np.array([[a1, 0, 0], [a2, a5, 0], [a3, a6, a9]]) / det_km
        self.bm = np.array([dzeta1, dzeta2, dzeta3])

    def _updated_result(self, step_number):
        x_fal, y_fal, z_fal = self.inputs[0].get_result(step_number)

        m_fal = np.array([x_fal, y_fal, z_fal])
        m = np.dot(self.Km_inv, m_fal - self.bm)

        modul = np.sqrt(m[0] * m[0] + m[1] * m[1] + m[2] * m[2])

        return m, modul


if __name__ == '__main__':
    mag = MagnetometrBlok()
    # mag.plik = 'dane/test1.txt'
    mag.load()
    print(mag.bledy)
