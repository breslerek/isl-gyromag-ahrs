import numpy as np


def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx


def main():
    t_0 = 100.0
    t_k = 172.0
    h = 0.25
    n = int((t_k - t_0) / h + 1)

    t = np.arange(n) * h + t_0
    t *= 1000000

    att = np.loadtxt('att.txt', delimiter=',', comments='#')
    giro = np.loadtxt('giro.txt', delimiter=',', comments='#')
    mag = np.loadtxt('mag.txt', delimiter=',', comments='#')

    att_025s = np.array([])
    giro_025s = np.array([])
    mag_025s = np.array([])

    for time in t:
        # a = np.array([
        #     find_nearest(att[:, 0], time),
        #     find_nearest(giro[:, 0], time),
        #     find_nearest(mag[:, 0], time)
        # ])
        # print(a)
        att_025s = np.concatenate((att_025s, att[find_nearest(att[:, 0], time), 1:]))
        giro_025s = np.concatenate((giro_025s, giro[find_nearest(att[:, 0], time), 1:]))
        mag_025s = np.concatenate((mag_025s, mag[find_nearest(att[:, 0], time), 1:]))

    att_025s = att_025s.reshape((n, 3))
    giro_025s = giro_025s.reshape((n, 3))
    mag_025s = mag_025s.reshape((n, 3))

    np.savetxt('att_025s.txt', att_025s, delimiter=', ', header='roll [deg], pitch [deg], yaw [deg]')
    np.savetxt('giro_025s.txt', giro_025s, delimiter=', ', header='v_x [rad/s], v_y [rad/s], v_z [rad/s]')
    np.savetxt('mag_025s.txt', mag_025s, delimiter=', ', header='mag_x [mGs], mag_y [mGs], mag_z [mGs]')

    t /= 1000000
    np.savetxt('time.txt', t - t_0, delimiter=', ', header='t [us]')


if __name__ == '__main__':
    main()
