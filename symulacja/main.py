#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test czy wszystkie biblioteki są poprawnie zainstalowane
"""

import numpy as np

from giroskop_blok import GiroskopBlok
from czujnik_blok import CzujnikBlok
from kalman_blok import KalmanBlok
from wykresy import katy_orientacji, natezenia_pola, katy_orientacji_att
from magnetometr_blok import MagnetometrBlok
from orientacja_mag_blok import OrientacjaMagBlok
import qmath as qm

def main():
    step_count = 289

    giro = GiroskopBlok()
    giro_czujnik = CzujnikBlok()
    # kalman = KalmanBlok()
    giro_czujnik.load('dane/pixhawk/giro_025s.txt')
    # kalman.inputs = [giro_czujnik]
    giro.inputs = [giro_czujnik]

    mag = MagnetometrBlok()
    mag.modul_wektora = 1.0
    mag.N = 20
    mag.load()
    mag_czujnik = CzujnikBlok()
    mag.inputs = [mag_czujnik]
    mag_czujnik.load('dane/pixhawk/mag_025s.txt')

    orient_mag = OrientacjaMagBlok()
    orient_mag.inputs = [mag]

    results_giro = np.array([])  # [fi, psi, teta]
    results_mag = np.array([])  # [x, y, z]
    results_orient = np.array([])
    results_slerp = np.array([])

    gamma = 0.25  # waga orientacji magnetycznej

    for step in range(step_count):
        results_giro = np.append(results_giro, qm.quat2att(giro.get_result(step)))
        results_mag = np.append(results_mag, mag.get_result(step)[0])
        results_orient = np.append(results_orient, qm.quat2att(orient_mag.get_result(step)))
        results_slerp = np.append(results_slerp, qm.quat2att(qm.slerp(
            giro.get_result(step),
            orient_mag.get_result(step),
            gamma)))

    results_giro = np.reshape(results_giro, (step_count, 3))
    results_mag = np.reshape(results_mag, (step_count, 3))
    t = np.loadtxt('dane/pixhawk/time.txt')
    katy_orientacji(t, results_giro)
    natezenia_pola(t, results_mag)

    att = np.loadtxt('dane/pixhawk/att_025s.txt', delimiter=',')
    katy_orientacji_att(t, att)

    results_orient = np.reshape(results_orient, (step_count, 3))
    katy_orientacji(t, results_orient)

    results_slerp = np.reshape(results_slerp, (step_count, 3))
    katy_orientacji(t, results_slerp)


if __name__ == '__main__':
    main()
