from base_block import BaseBlock


class BenizBlock(BaseBlock):
    def __init__(self):
        super(BenizBlock, self).__init__()

    def _updated_result(self, step_number):
        velocities = self.inputs[0].get_result(step_number)
        result = velocities * 500 * 1024
        return result
