from base_block import BaseBlock
from czujnik_blok import CzujnikBlok
import numpy as np
from numpy import matmul
from numpy import array
from scipy.integrate import odeint
import matplotlib.pyplot as plt
import math


class GiroskopBlok(BaseBlock):
    def __init__(self):
        super(GiroskopBlok, self).__init__()
        self.A0 = array([0.0, 0.0, 0.0, 1.0])
        self.last_MA = self.A0
        self.t = 0.25

    def deriv(self, A, t, Ab):
        return np.dot(Ab, A)+0.001*abs(1-A*A)*A

    def MatrixBM3(self, v_fi, v_psi, v_teta):
        Omega = 1/2 *array([[0., -v_fi, v_psi, -v_teta],
                       [v_fi, 0., v_teta, -v_psi],
                       [v_psi, -v_teta, 0., v_fi],
                       [v_teta, v_psi, -v_fi, 0.]])
        return (Omega)

    def _updated_result(self, step_number):
        # wczytanie danych do obliczeń
        v_fi, v_psi, v_teta = self.inputs[0].get_result(step_number)
       # self.fi += v_fi * self.t
       # self.psi += v_psi * self.t
       # self.teta += v_teta * self.t
       # stowrzenie z nich macierzy
        Ab = self.MatrixBM3(v_fi, v_psi, v_teta)
        #time = np.arange(0., 1., self.t)
        time = array([0., 1.33])
        MA = odeint(self.deriv, self.last_MA, time, args=(Ab,))
        self.last_MA = MA[1]
        print(MA[1])
        return MA[1]

if __name__ == '__main__':
    giro = GiroskopBlok()
    giro_czujnik = CzujnikBlok()
    giro.inputs = [giro_czujnik]
    giro_czujnik.load('dane/obrot_teta_10s.txt')
#    data_kal = np.loadtxt('dane/kal_test1.txt', delimiter=',', comments='#')

    N = 30
    test1 = np.zeros((N, 4))
    test2 = np.zeros((N, 4))
    test3 = np.arange(N)
    for i in range(N):
        print("krok {}: giro: {}".format(i, giro.get_result(i)))
   #     test1[i] = kal.get_result(i)
        test2[i] = giro.get_result(i)

    plt.plot(test3, test2,test1, test1[:, 0])
    plt.xlabel('Krok czasowy [s]')
    plt.ylabel('Wartosc kwaterionów')
    plt.title('Prezentacja Giroskopów')
    plt.legend(['q0', 'q1', 'q2', 'q3'])
    plt.grid()

    plt.show()

