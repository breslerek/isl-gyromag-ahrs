#!/usr/bin/env python3
# -*- coding: utf-8 -*-


class BaseBlock:
    def __init__(self):
        self.inputs = None
        self._result = None
        self._last_step_updated = -1

    def get_result(self, step_number):
        if step_number == self._last_step_updated:
            return self._result
        else:
            self._result = self._updated_result(step_number)
            self._last_step_updated = step_number
            return self._result

    def _updated_result(self, step_number):
        """
        W klasach pochodnych nadpisać tę metodę żęby robić inne obliczenia,
        na razie po prostu zwraca numer kroku
        """
        # raise NotImplemented
        # return self.inputs[0].get_result(step_number)
        return step_number
