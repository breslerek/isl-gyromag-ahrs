from base_block import BaseBlock
from czujnik_blok import CzujnikBlok
import numpy as np
from numpy.linalg import inv
import matplotlib.pyplot as plt

class KalmanBlok(BaseBlock):
    def __init__(self):
        super(KalmanBlok, self).__init__()
        # definiujemy równanie stanu jako x'=F*x+w. Szukamy początkowo wartości stałej, a wiec x'=0. Co więcej, wtedy F=1
        self.F = np.identity(3)
        self.F[0][0]=1.0000
        # definiujemy równanie pomiaru w postaci z=H*x+v. Ogólnie chcemy w tym przypadku
        # żeby nasza wartość była równa pomiarom, czyli z=x, a więc definiujemy H=1
        self.H = np.identity(3)
        # macierz kowariancji błędu pomiaru <- sformułować jak najszybciej!
        self.R = np.identity(3)
        self.R[0][0]=0.0001
        # macierz kowariancji błędu wektora stanu
        self.Q = 0.00001 * np.identity(3)
        # macierz kowariancji błędu estymaty
        self.P = np.identity(3)
        self.P[0][0] = 999999

        self.velocity = None
        #self.velocity = np.zeros((3, 1))

        self.velocity_past=np.array([[0], [0], [0]])

        # self.velocity_predicted = None
        self.velocity_predicted = np.zeros((3, 1))

        # wzmocnienie Kalmana
        self.K = np.ones((3, 3))
        self.I = np.identity(3)



    def _updated_result(self, step_number):
        # przyjmij pierwszy pomiar jako początkowa wartosc
        if self.velocity is None:
            self.velocity = self.inputs[0].get_result(step_number)
            self.velocity = np.reshape(self.velocity, (3,1))

        if self.velocity_predicted is None:
            self.velocity_predicted = self.inputs[0].get_result(step_number)
            self.velocity_predicted = np.reshape(self.velocity_predicted, (3,1))

        v_fi, v_psi, v_teta = self.inputs[0].get_result(step_number)
        velocity_data = np.array([[v_fi], [v_psi], [v_teta]])
        # definiujemy wzmocnienie Kalmana dla danej iteracji
        self.K = np.matmul(self.P, self.H.transpose())
        HPH = np.matmul(self.H, np.matmul(self.P, self.H.transpose())) + self.R
        self.K = np.matmul(self.K, inv(HPH))
        # defijiujemy zmienną obserwowaną dla danej iteracji
        Z = velocity_data
        # defijiujemy nową przewidywana daną, na podstawie jej wczesniejszej wartości
        self.velocity = self.velocity_predicted + np.matmul(self.K, Z - np.matmul(self.H, self.velocity_predicted))
        # ponownie wyliczamy macierz kowariancji
        self.P = np.matmul(self.I - np.matmul(self.K, self.H), self.P)
        # wyliczamy przyszła kowariancje
        self.P = np.matmul(self.F, np.matmul(self.P, self.F.transpose()))
        # wyliczamy przyszałą wartość
        self.velocity_predicted = np.matmul(self.F, self.velocity)

        # delta=(self.velocity-self.velocity_past)/self.delta_t
        # delta=np.arctan(delta)
        # self.F=np.identity(3)
        # self.F[0][0]=self.F[0][0]+np.sin(delta[0])*self.delta_t
        # print(self.F)
        # self.velocity_past=self.velocity

        velocity_output = self.velocity
        return np.transpose(velocity_output)


if __name__ == '__main__':
    kal = KalmanBlok()
    mag_czujnik = CzujnikBlok()
    kal.inputs = [mag_czujnik]
    mag_czujnik.load('dane/obrot_teta_10s_mag_szum.txt')

    N = 1000
    test1 = np.zeros((N, 3))
    test2 = np.zeros((N, 3))
    test3 = np.arange(N)

    for i in range(N):
        print("krok {}: mag: {}, kal: {}".format(i, mag_czujnik.get_result(i), kal.get_result(i)))
        test1[i] = kal.get_result(i)
        test2[i] = mag_czujnik.get_result(i)

    mag_czysty=np.loadtxt('dane/obrot_teta_10s_mag_czysty.txt', delimiter=',', comments='#')
    mag_zaszum=np.loadtxt('dane/obrot_teta_10s_mag_zaszumiony.txt', delimiter=',', comments='#')

    plt.plot(test3, mag_zaszum[:, 0], test3, test1[:, 0]+mag_czysty[:, 0])
    plt.xlabel('Czas [s]')
    plt.ylabel('Wartosc sygnału')
    plt.title('Prezentacja filtracji Kalmana')
    plt.legend(['Raw', 'Kalman'])
    plt.grid()

    plt.show()

