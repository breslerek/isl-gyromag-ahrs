import numpy as np


def mag_obrot_teta_10s(n=10):
    teta = np.arange(n) / n * 2 * np.pi
    return np.transpose(np.stack((teta*teta, np.sin(teta), np.zeros(n))))


def randomize_magnitude(vectors: np.ndarray, min, max):
    n = vectors.shape[0]
    multiplier = np.random.uniform(min, max, n)
    return (vectors.T * multiplier).T


def save(filename: str, array: np.ndarray, comment: str):
    with open(filename, 'w') as file:
        file.write('# ' + comment + '\n')

        for line in array:
            words = ['{:.4f}'.format(num) for num in line]
            file.write(', '.join(words) + '\n')


if __name__ == '__main__':
    mag = mag_obrot_teta_10s(1000)
    noisy_mag = randomize_magnitude(mag, 0.8, 1.2)
    noise = noisy_mag - mag
    save('dane/obrot_teta_10s_mag_zaszumiony.txt', noisy_mag, 'MagX, MagY, MagZ')
    save('dane/obrot_teta_10s_mag_czysty.txt', mag, 'MagX, MagY, MagZ')
    save('dane/obrot_teta_10s_mag_szum.txt', noise, 'MagX, MagY, MagZ')
