import numpy as np
from base_block import BaseBlock


class CzujnikBlok(BaseBlock):
    def __init__(self):
        super(CzujnikBlok, self).__init__()
        self.data = np.array([])

    def load(self, filename):
        self.data = np.loadtxt(filename, delimiter=',', comments='#')

    def _updated_result(self, step_number):
        return self.data[step_number]
