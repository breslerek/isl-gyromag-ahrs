%Karol Bresler

\subsection{Filtr Kalmana - Karol Bresler}

Filtr Kalmana został po raz pierwszy zaproponowany przez R.E. Kalmana w 1960 roku. Metoda ta opiera się na zbieraniu informacji w czasie (zawierających szumy i inne niedokładności). Następnie, na podstawie zebranych danych szacowana jest wartość oczekiwanej zmiennej, w taki sposób, aby była ona jak najwierniejsza rzeczywistości. Na tym etapie właśnie przebiega najważniejszy etap całego algorytmu - na podstawie zebranych danych, estymujemy jej przyszłą wartość (np. położenie) w taki sposób, by była ona jak najbardziej zbliżona do "przyszłego" pomiaru. Dokonujemy tego dzięki minimalizacji błędów. W skrócie więc, staramy się znaleźć pewien wzorzec na nasze dane.  

Filtr ten znalazł szerokie zastosowanie przy obliczaniu orbit satelitów, namierzaniu poruszających się obiektów czy też w ogólnej nawigacji. Metoda została opisana na podstawie \cite{Kalman}.

\subsubsection{Model matematyczny} 

\paragraph{Wartość stała w czasie}


Na samym początku rozważymy najprostszy możliwy przypadek, a mianowicie gdy nasza zmienna jest stała w czasie. Naszym zadaniem jest więc takie dobranie parametrów i sposobu, aby przewidywana zmienna zbiegała się ze zmierzoną wartością stałą. 

Załóżmy zatem, iż skalar $x$ jest stały w czasie. W danym przedziale czasu wykonujemy $n$ pomiarów skalara $x$, które obciążone są pewnym błędem (szumy, niedokładności). Zebrane wartości pomiaru (już obciążone błędem) oznaczymy jako $\hat{y}_i$. Możemy zatem przyjąć iż najlepszym oszacowaniem naszej zmiennej będzie średnia ze wszystkich pomiarów. Oszacowanie to oznaczymy jako $\hat{x}_n$:

\begin{align}
\hat{x}_n=\frac{1}{n}\cdot \sum_{i=1}^{i=n}\hat{y}_i
\end{align}

Po dokonaniu kolejnego pomiaru ($n+1$) uzyskamy oczywiście analogiczną formułę:

\begin{align}
\hat{x}_{n+1}=\frac{1}{n+1}\cdot \sum_{i=1}^{i=n+1}\hat{y}_{i+1}
\end{align}

Używając tych dwóch równań, możemy przekształcić wyrażenie na $\hat{x}_{n+1}$ do następującej postaci:

\begin{align}
\hat{x}_{n+1}=\hat{x}_{n}\cdot \frac{n}{n+1}+ \frac{\hat{y}_{i+1}}{n+1}
\end{align}
\begin{align}
\hat{x}_{n+1}=\hat{x}_{n}+ \frac{1}{n+1}\cdot[\hat{y}_{i+1}-\hat{x}_{n}] 
\end{align}

Uzyskane zależności możemy zrozumieć w następujący sposób: jeśli oszacowaliśmy wartości $x$ poprzez $\hat{x}_n$  i pojawia się nowy pomiar, to możemy z niego skorzystać by zaktualizować naszą "przewidywaną" wartość do $\hat{x}_{n+1}$. Zrobimy to poprzez dodanie do $\hat{x}_n$ różnicy pomiędzy nową zmierzona wartością a poprzednią estymacją $\hat{x}_n$ (człon $\hat{y}_{i+1}-\hat{x}_{n}$). Szukamy zatem różnicy pomiędzy wartością zmierzoną, a wartością jaką przewidywaliśmy że będzie ona mieć. Różnica ta będzie przemnożona przez odpowiednią wartość skalującą (człon $\frac{1}{n+1}$). 

Właśnie ten algorytm aktualizacji oszacowania w momencie pojawienia się nowego pomiaru w celu uzyskania lepszego oszacowania jest podstawą funkcjonowania filtru Kalmana. 

\paragraph{Wartość zmienna w czasie}

W tym przypadku będziemy ponownie rozważać skalar $x$, którego wartości odnotowywane są w dyskretnych punktach czasu. Zakładamy teraz iż zmiana $x$ pomiędzy $n$-tym a $n+1$ pomiarem może być przedstawiona w następujący sposób:

\begin{align}
\hat{x}_{n+1}=\phi_n\cdot x_n+w_n
\end{align}

$w_n$ tutaj jest szumem jaki występuje w czasie pomiaru, zaś $\phi_n$ jest "przelicznikiem" w którym zawarte są fizyczne właściwości modelu - czyli jak znając poprzedni stan, uzyskać stan kolejny. Przyjmując zatem, że oszacowanie $x_n$ oznaczymy jako $\hat{x}_{n}(+)$. Użyty tu $(+)$ oznacza iż jest już to wartość zaktualizowana po odnotowaniu pomiaru.  Najlepsze oszacowanie wartości $x_{n+1}$ (oznaczonej jako $\hat{x}_{n+1}(-)$) będzie miało postać:

\begin{align}
\hat{x}_{n+1}(-)=\phi_n\cdot x_n(+)
\end{align}

Do następnego etapu rozważań potrzebna będzie nam wariancja. Wariancja jest to klasyczna miara zróżnicowania danego zbioru wartości. Wyliczana jest jak jako średnia arytmetyczna z kwadratów odchyleń poszczególnych wartości cechy od średniej arytmetycznej całego zbioru. Można ją zatem opisać wzorem:

\begin{align}
\sigma^2=\dfrac{\sum_{j=1}^{j=n}(x_i^2-\bar{X})}{n}=E[(\bar{X}-x_i)^2]
\end{align}

Jeśli $\hat{x}_n(+)$ może zostać scharakteryzowana za pomocą wariancji $P_n=E[(\bar{x}_n-x_n)^2]$, to wariancja zmiennej oczekiwanej $\bar{x}_{n+1}$ będzie zatem powiększoną wariancją zmiennej $\bar{x}_{n}$ o człon $\phi_n^2\cdot P_n(+)$. Analogicznie musimy wziąć pod uwagę niepewności od naszego szumu $w_n$, które będą równe $Q_n=E(w_n^2)$. Całkowita wariancja zmiennej $\hat{x}_{n+1}$ będzie zatem równa:

\begin{align}
P_{n+1}= \phi_n^2\cdot P_n(+)+Q_n
\end{align}

Dokonujemy zatem pomiaru wartości $x_{n+1}$, którą nazwiemy $y_{n+1}$. Zakładamy liniową zależność pomiędzy naszym pomiarem a mierzoną wartością:

\begin{align}
y_{n+1}=H_{n+1}\cdot x_{n+1}
\end{align}

Zapisana wyżej wartość jest prawdziwa dla idealnego przypadku. W rzeczywistości będziemy musieli także uwzględnić występujące szumy pomiarowe. Ostatecznie będziemy mieć:

\begin{align}
\hat{y}_{n+1}=H_{n+1}\cdot x_{n+1}+v_{n+1}
\end{align}

Wariancja naszego pomiaru może być opisana następująco:

\begin{align}
R_{n+1}=E[v_{n+1}^2]
\end{align}

Po wykonaniu pomiaru będziemy zatem starać się znaleźć zaktualizowaną wartość $\hat{x}_{n+1}(-)$, którą oznaczymy $\hat{x}_{n+1}(+)$, która może zostać wyliczona w następujący sposób:

\begin{align}
\hat{x}_{n+1}(+)=\hat{x}_{n+1}(-)+K_{n+1}\cdot [\hat{y}_{n+1}-H_{n+1}\cdot \hat{x}_{n+1}(-)]
\end{align}

Wartość $K_{n+1}$ nazywana jest wzmocnieniem Kalmana. Będziemy się starać znaleźć taką wartość $K_{n+1}$, aby wariancja zmiennej $x_{n+1}(+)$ oznaczona jako $P_{n+1}(+)$ była jak najmniejsza. Dzięki temu nowo otrzymana zmienna $x_{n+1}(+)$ będzie miała najmniejszy możliwy błąd. Zapiszmy zatem zmiany w zmiennych za pomocą znaku $\Delta$. Różnica ta będzie równa:

\begin{align}
\Delta\hat{x}_{n+1}(+)=(1-K_{n+1}\cdot H_{n+1})\Delta \hat{x}_{n+1}(-)+K_{n+1} \cdot\Delta \hat{y}_{n+1}
\end{align}

A zatem:

\begin{align}
P_{n+1}(+)=(1-K_{n+1}\cdot H_{n+1})\cdot P_{n+1}(-)K_{n+1}^2\cdot R_{n+1}
\end{align}

Następnie różniczkujemy po $K_{n+1}$ i uzyskaną zależność przyrównujemy do zera (szukamy po prostu minimum otrzymanej funkcji). Po pogrupowaniu wyrazów uzyskamy następującą zależność na zyska Kalmana:

\begin{align}
K_{n+1}=\dfrac{P_{n+1}(-)\cdot H_{n+1}}{H_{n+1}^2\cdot P_{n+1}(-)+R_{n+1}}
\end{align}

Dla takiej wartości $K_{n+1}$ otrzymujemy następującą wariancję całego układu:

\begin{align}
P_{n+1}(+)=(1-K_{n+1}\cdot H_{n+1})\cdot P_{n+1}(-)
\end{align}



\subsubsection{Podsumowanie}

Zaprezentowany tu został podstawowy algorytm działania filtra Kalmana, który może następnie zostać zastosowany do szerokiej gamy aplikacji. 


